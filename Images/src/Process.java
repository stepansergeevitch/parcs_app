import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.lang.Math;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;    
import java.io.InputStream;
import parcs.*;

class CustomFilenameFilter implements FilenameFilter{
    static String[] extensions = new String[]{
        "gif", "png", "bmp"
    };  

    @Override
    public boolean accept(File dir, String name) {
        for (String ext : extensions) {
            if (name.endsWith("." + ext)) {
                return (true);
            }
        }
        return (false);
    }
}

public class Process {
    static private int points_number = 2;
    static private int image_number = 100;

    public static void main(String[] args) throws Exception {
        task curtask = new task();
        curtask.addJarFile("ImageProcessor.jar");
        ArrayList<byte[]> images = get_all_images(image_number);
        AMInfo info = new AMInfo(curtask, null);
        int step = (int)Math.ceil(image_number / points_number);
        ArrayList<channel> channels = new ArrayList<channel>();
        for (int pos = 0; pos < image_number; pos += step) {
            point p = info.createPoint();
            channel c = p.createChannel();
            p.execute("ImageProcessor");
            c.write(step);
            for (byte[] img: images.subList(pos, Math.min(pos + step, image_number))){
                c.write(img);
            }
            channels.add(c);
        }
        System.out.println("Waiting for result...");
        ArrayList<byte[]> result_images = new ArrayList<byte[]>();
        for (channel c: channels) {
            for (int i = 0;i < step;i ++) {
                result_images.add((byte[])c.readObject());
            }
        }
        curtask.end();
        write_images(result_images);
    }

    static String file_directory = "/home/stepan/Pictures/";
    static CustomFilenameFilter image_filter;

    public static ArrayList<byte[]> get_all_images(int max) {
        ArrayList<byte[]> images = new ArrayList<byte[]>();
        int cnt = 0;
        File dir = new File(file_directory + "parcs_images");
        if (dir.isDirectory()) {
            for (File f : dir.listFiles(image_filter)) {
                try {
                    BufferedImage img = ImageIO.read(f);
                    images.add(image_to_bytes(img));
                    cnt += 1;
                } catch (IOException e) {}
                if (cnt >= max) {
                    break;
                }
            }
        }
        return images;
    }

    public static void write_images(ArrayList<byte[]> images) {
        int cnt = 0;
        String dir_path = file_directory + "parcs_result/";
        for (byte[] img: images) {
            File outputfile = new File(dir_path + Integer.toString(cnt));
            try {
                ImageIO.write(bytes_to_image(img), "png", outputfile);
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
            cnt += 1;
        }
    }

    public static byte[] image_to_bytes(BufferedImage img) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try{
            ImageIO.write(img, "jpg", baos);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            baos.flush();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        byte[] imageInByte = baos.toByteArray();
        try {
            baos.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return imageInByte;
    }

    public static BufferedImage bytes_to_image(byte[] img) {
        InputStream in = new ByteArrayInputStream(img);
        BufferedImage bImageFromConvert = null;
        try {
            bImageFromConvert = ImageIO.read(in);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return bImageFromConvert;
    }
}
