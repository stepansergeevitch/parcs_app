import java.util.List;
import java.util.ArrayList;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import javax.imageio.ImageIO;
import java.io.ByteArrayOutputStream;
import java.io.ByteArrayInputStream;    
import java.io.InputStream;
import java.io.IOException;
import parcs.*;

public class ImageProcessor implements AM {
    public void run(AMInfo info) {
        ArrayList<byte[]> images = new ArrayList<byte[]>();
        int cnt = (int)info.parent.readObject();
        for (int i = 0; i < cnt; i++) {
            images.add((byte[])info.parent.readObject());
        }
        System.out.println("Processing started.");
        List<byte[]> result_images = process_images(images);
        System.out.println("Processing finished.");
        for (byte[] img: result_images) {
            info.parent.write(img);    
        }
    }

    private ArrayList<byte[]> process_images(ArrayList<byte[]> images) {
        float[] blur_matrix = {
            0.111f, 0.111f, 0.111f, 
            0.111f, 0.111f, 0.111f, 
            0.111f, 0.111f, 0.111f, 
        };
        BufferedImageOp op = new ConvolveOp( new Kernel(3, 3, blur_matrix));
        ArrayList<byte[]> result_images = new ArrayList<byte[]>();
        for(byte[] img: images) {
            BufferedImage real_image = bytes_to_image(img);
            BufferedImage blurred_image = op.filter(real_image, real_image);
            result_images.add(image_to_bytes(blurred_image));
        }
        return result_images;
    }

    public static byte[] image_to_bytes(BufferedImage img) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(img, "jpg", baos);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        try {
            baos.flush();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        byte[] imageInByte = baos.toByteArray();
        try {
            baos.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return imageInByte;
    }

    public static BufferedImage bytes_to_image(byte[] img) {
        InputStream in = new ByteArrayInputStream(img);
        BufferedImage bImageFromConvert = null; 
        try {
            bImageFromConvert = ImageIO.read(in);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return bImageFromConvert;
    }
}
